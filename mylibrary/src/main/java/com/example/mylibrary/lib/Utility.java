package com.example.mylibrary.lib;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Utility {


    public static List<AppDetailsList> sortAlphabetically(List<AppDetailsList> apps) {

        Collections.sort(apps, new Comparator<AppDetailsList>() {
            @Override
            public int compare(AppDetailsList item1, AppDetailsList item2) {
                return item1.getApp_name().compareToIgnoreCase(item2.getApp_name());
            }
        });

        return apps;
    }




}
