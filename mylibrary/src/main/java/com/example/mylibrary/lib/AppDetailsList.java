package com.example.mylibrary.lib;

import android.graphics.drawable.Drawable;

public class AppDetailsList {
   public String app_name;
    public String package_name;
    public  Drawable icon;
    public String main_activity_class_name;
    public int version_code;
    public String version_name;

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getMain_activity_class_name() {
        return main_activity_class_name;
    }

    public void setMain_activity_class_name(String main_activity_class_name) {
        this.main_activity_class_name = main_activity_class_name;
    }

    public int getVersion_code() {
        return version_code;
    }

    public void setVersion_code(int version_code) {
        this.version_code = version_code;
    }

    public String getVersion_name() {
        return version_name;
    }

    public void setVersion_name(String version_name) {
        this.version_name = version_name;
    }
}
