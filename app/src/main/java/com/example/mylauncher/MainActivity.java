package com.example.mylauncher;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;

import com.example.mylibrary.lib.AppDetailsList;
import com.example.mylibrary.lib.Utility;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private PackageManager manager;
    PackageInfo pInfo;
    private List<AppDetailsList> apps;
    private RecyclerView mRecyclerView;
    private AppListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getAllAppDetails();

        //Using library functions to sort
        apps = Utility.sortAlphabetically(apps);

        mAdapter = new AppListAdapter(this, apps, manager);
        mAdapter.notifyDataSetChanged();
        // Connect the adapter with the RecyclerView.
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }


    private void getAllAppDetails() {
        manager = getPackageManager();
        apps = new ArrayList<AppDetailsList>();

        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
        try {
            for (ResolveInfo ri : availableActivities) {
                pInfo = manager.getPackageInfo(getPackageName(), 0);

                AppDetailsList app = new AppDetailsList();
                app.app_name = (String) ri.loadLabel(manager);
                app.package_name = ri.activityInfo.packageName;
                app.main_activity_class_name = manager.getLaunchIntentForPackage(app.package_name).getComponent().getClassName();
                app.version_name = manager.getPackageInfo(app.package_name, 0).versionName;
                app.version_code = manager.getPackageInfo(app.package_name, 0).versionCode;
                app.icon = ri.activityInfo.loadIcon(manager);
                apps.add(app);

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }


}
