package com.example.mylauncher;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mylibrary.lib.AppDetailsList;

import java.util.List;

public class AppListAdapter extends RecyclerView.Adapter<AppListAdapter.AppViewHolder> {
    private List<AppDetailsList> appsList;
    private LayoutInflater mInflater;
    private Context context;
    private PackageManager manager;

    @NonNull
    @Override
    public AppViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.list_item, parent, false);
        return new AppViewHolder(mItemView, this, manager, context);
    }

    @Override
    public void onBindViewHolder(@NonNull AppViewHolder holder, int position) {
        AppDetailsList mCurrent = appsList.get(position);
        holder.app_name.setText(mCurrent.app_name);
        holder.package_name.setText(mCurrent.package_name);
        holder.version_code.setText(String.valueOf(mCurrent.version_code));
        holder.version_name.setText(mCurrent.version_name);
        holder.main_activity_class_name.setText(mCurrent.main_activity_class_name);
        holder.icon.setImageDrawable(mCurrent.icon);

    }

    @Override
    public int getItemCount() {
        return appsList.size();
    }

    public AppListAdapter(Context context, List<AppDetailsList> appsList, PackageManager manager) {
        mInflater = LayoutInflater.from(context);
        this.appsList = appsList;
        this.context = context;
        this.manager = manager;

    }

    public class AppViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final TextView app_name;
        public final TextView package_name;
        public final TextView main_activity_class_name;
        public final TextView version_code;
        public final TextView version_name;
        public final ImageView icon;
        public final AppListAdapter mAdapter;
        PackageManager packageManager;
        Context ctx;

        public AppViewHolder(View itemView, AppListAdapter adapter, PackageManager manager, Context context) {
            super(itemView);
            itemView.setOnClickListener(this);
            app_name = (TextView) itemView.findViewById(R.id.item_app_name);
            package_name = (TextView) itemView.findViewById(R.id.item_app_label);
            main_activity_class_name = (TextView) itemView.findViewById(R.id.main_activity_class_name);
            version_code = (TextView) itemView.findViewById(R.id.version_code);
            version_name = (TextView) itemView.findViewById(R.id.version_name);
            icon = (ImageView) itemView.findViewById(R.id.item_app_icon);
            this.mAdapter = adapter;
            this.packageManager = manager;
            this.ctx = context;
        }

        @Override
        public void onClick(View v) {

            int pos = this.getAdapterPosition();

            Intent i = packageManager.getLaunchIntentForPackage(appsList.get(pos).package_name);
            ctx.startActivity(i);

        }
    }


}
